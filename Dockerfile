
## imagen base padrao arquitetura Java/SpringBoot
FROM registry.pmf.sc.gov.br:5000/infra/docker-files/oraclejdk8-alpine:01.01.00-RC06

MAINTAINER ungp.pmf@softplan.com.br

USER root

## instalando envplate para interpolar valores de variáveis de ambiente
RUN apk --update add ca-certificates wget \
 && update-ca-certificates \
 && curl -sLo /usr/bin/ep https://github.com/kreuzwerker/envplate/releases/download/1.0.0-RC1/ep-linux \
 && chmod +x /usr/bin/ep

## atualizando data
RUN apk add --update tzdata
ENV TZ=America/Sao_Paulo

## definindo a variavel de ambiente passada no momento do build da imagem
ARG version

## definindo variaveis locais
ENV APP_VERSION $version
ENV APP_HOME "/home/softplan"
ENV APP_NAME "ci-cd-sample-java-"$version.jar
ENV APM_APP_NAME "ci-cd-sample-java"

## criando diretorio padrao do app e otimizacao de cache da imagem
RUN mkdir -p $APP_HOME
RUN rm -rf /var/cache/apk/*

## copiando os artefatos do projeto para a imagem que está sendo criada
COPY ./target/resources/application.properties $APP_HOME/
COPY ./target/resources/messages.properties $APP_HOME/
COPY ./target/resources/jolokia-access.xml $APP_HOME/
COPY ./target/resources/logback.xml $APP_HOME/
COPY ./target/ci-cd-sample-java-$APP_VERSION.jar $APP_HOME/

## permissao de arquivo
RUN chown softplan $APP_HOME/*.properties

## adicionado as variaveis incluidas em JAVA_OPTS no arquivo run-docker.sh
RUN sed -i 's/$JAVA_OPTS/$JAVA_OPTS $DOCKER_OPTS/g' $APP_HOME/envs/run-docker.sh

## definindo aquivo e diretorio de log padrao
ENV LOG_DIR="/var/log/ci-cd-sample-java"
RUN mkdir -p $LOG_DIR && chown softplan. $LOG_DIR

## interpolando valores de variaveis
ENV JAVA_OPTS="-server -Xms512m -Xmx512m -XX:MaxMetaspaceSize=512m -XX:+UseG1GC -Duser.timezone=America/Sao_Paulo -Duser.language=pt -Duser.country=BR  -Dfile.encoding=UTF-8 -Dspring.profiles.default=docker"
ENV DOCKER_OPTS="-Dloader.path=file://$APP_HOME"

USER softplan

## definindo instrucoes de inicio do container
CMD "/usr/bin/ep" "/home/softplan/apm/*.properties" "*.properties" "--" "$APP_HOME/envs/run-docker.sh" 
