# Sample CI/CD Java 
 

 Exemplo de pipeline de desenvolvimento de um projeto Java/SpringBoot usando:
 * [CI/CD - GitLab](https://docs.gitlab.com/ee/ci/yaml/)
 * [Maven](https://maven.apache.org/)
 * [Versionamento semântico](https://semver.org/lang/pt-BR/) 
 * [Fluxo GitFlow](https://nvie.com/posts/a-successful-git-branching-model/)
 * [Containers Docker](https://www.docker.com/resources/what-container)
 * [Rancher](https://rancher.com/what-is-rancher/overview/)

## Premissas:

* Ter o arquivo  .m2/settings.xml configurado na raiz do projeto. 
* Ter o arquivo Dockerfile na raiz do projeto. 
* Ter o ambiente/stack/serviço criado no Rancher.
* Ter as seguintes variaveis de ambiente configuradas no GitLab:
  * CI_GIT_URL - *ex: gitlab.com*  
  * CI_GIT_USER_NAME - *ex: git* 
  * CI_GIT_USER_PASS - *ex: 123456*
  * CI_REGISTRY_URL - *ex: registry.gitlab.com*
  * CI_REGISTRY_USER - *ex: docker*
  * CI_REGISTRY_PASSWORD - *ex: 123456*
  * DOCKER_AUTH_CONFIG - *ex: { "auths": { "registry.gitlab.com": {"auth": "ZG9ja2VyOmNqOVVpYTBRdzZhYjJkWU5lakVhcW9Ob0YyWUNBdXpkUll2RWpMZDFzb3c="} } }*
  * RANCHER_ACCESS_KEY - *ex: 240B1581A892FE9B88B7*
  * RANCHER_SECRET_KEY - *ex: Gi53UNpbJJhwoZXsZXEUkZo2bWfMPQXtfDBDi3P3*
  * RANCHER_URL - *ex: https://sua.instalacao.do.rancher*
